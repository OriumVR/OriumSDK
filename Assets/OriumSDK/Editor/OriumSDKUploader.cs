﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEditor;

public static class OriumSDKUploader
{
    public static void UploadSetBundle()
    {
        EditorUtility.DisplayProgressBar("Uploading Set Bundle", "Loading Set Bundle", 0.0f);

        FtpWebRequest request = (FtpWebRequest) WebRequest.Create("ftp://www.orium-vr.com/testbundle");

        request.Method = WebRequestMethods.Ftp.UploadFile;

        // TODO: Change Upload Credentials

        request.Credentials = new NetworkCredential("administrator", "4nim4tion");

        using (StreamReader sourceStream = new StreamReader("Assets\\AssetBundles\\testbundle"))
        {
//        byte [] fileContents = new byte[sourceStream.]Encoding.ASCII.GetBytes(sourceStream.ReadToEnd());
//        sourceStream.Close();
//        request.ContentLength = fileContents.Length;

            try
            {
                EditorUtility.DisplayProgressBar("Uploading Set Bundle", "Connecting To Orium", 0.0f);

                Stream requestStream = request.GetRequestStream();

                int length = (int)sourceStream.BaseStream.Length;
                byte[] buffer = new byte[1024];
                for (int i = 0; i < length; i += 1024)
                {
                    if (EditorUtility.DisplayCancelableProgressBar("Uploading Set Bundle", "Uploading...", ((float)i) / ((float)length)))
                    {
                        request.Abort();
                        break;
                    }

                    sourceStream.BaseStream.Read(buffer, 0, Math.Min(1024, length - i));
                    requestStream.Write(buffer, 0, Math.Min(1024, length - i));
                }
                EditorUtility.ClearProgressBar();

                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                UnityEngine.Debug.Log("Upload Set Bundle Complete \n" + response.StatusDescription);

                response.Close();
            }
            catch (SocketException)
            {
                UnityEngine.Debug.LogError("Failed to connect to Orium");
                EditorUtility.ClearProgressBar();
            }
            catch (WebException exception)
            {
                UnityEngine.Debug.LogError("Upload Set Bundle Failed \n" + exception.ToString());
                EditorUtility.ClearProgressBar();
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.LogError("Upload Set Bundle Failed \n" + exception.ToString());
                EditorUtility.ClearProgressBar();
            }
        }
    }

}

