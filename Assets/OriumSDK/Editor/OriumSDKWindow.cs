﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;

public class OriumSDKWindow : EditorWindow {
    
    [MenuItem ("Window/Orium SDK")]
    public static void  ShowWindow () {
        EditorWindow.GetWindow(typeof(OriumSDKWindow));
    }

    private OriumSDKAssembler _assembler;

    private Texture _logoTexture;

    void OnEnable()
    {
        _assembler = new OriumSDKAssembler();
        titleContent = new GUIContent("Orium SDK");
        minSize = new Vector2(170, 150);
    }

    void Update()
    {
        _assembler.Update();
    }

    void BuildSetBundle()
    {
        // TODO: WARN if the set bundle contains any scripts?

        TryDeleteFile("Assets\\AssetBundles\\testbundle");
        TryDeleteFile("Assets\\AssetBundles\\testbundle.manifest");
        TryDeleteFile("Assets\\SetBundle\\SetBundleScripts.bytes");

        File.Copy("Assets\\SetBundle\\SetBundleScripts.dll", "Assets\\SetBundle\\SetBundleScripts.bytes");

        AssetDatabase.Refresh();

        AssetImporter.GetAtPath("Assets\\SetBundle\\SetBundleScripts.bytes").assetBundleName = "testbundle";

        // TODO: Multiple builds for different target platforms!
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    bool TryDeleteFile(string path)
    {
        try
        {
            File.Delete(path);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    void OnGUI()
    {
        if (_logoTexture == null)
        {
            _logoTexture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/OriumSDK/Editor/Textures/OriumSDKLogo.png");
        }

        GUILayout.Label(_logoTexture);
        GUILayout.Label("Pre-Alpha Version 0.1");

        if (GUILayout.Button("Build Script Assembly"))
        {
            _assembler.GenerateAndBuild();
        }

        if (GUILayout.Button("Build Set Bundle"))
        {
            BuildSetBundle();
        }

        if (GUILayout.Button("Upload Set Bundle"))
        {        
            OriumSDKUploader.UploadSetBundle();
        }
    }
}
