﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEditor;

public class OriumSDKAssembler
{
    private Process _genProc;
    private Process _buildProc;

    public void Update()
    {
        if (_genProc != null && _genProc.HasExited)
        {
            _genProc = null;
            OnProjectGenerated(this, null);
        }
    }

    public static Process GenerateProject()
    {
        string[] files = Directory.GetFiles("Assets\\SetBundle","*.cs",SearchOption.AllDirectories);

        Process genProc = new Process();
        genProc.StartInfo.FileName = "Assets\\OriumSDK\\CSProjGenerator\\CSProjGenerator.exe";
        //genProc.StartInfo.WorkingDirectory = "Assets\\Orium\\CSProjGenerator\\";
        if (files.Length > 0)
        {
            genProc.StartInfo.Arguments = "\"" + string.Join("\" \"", files) + "\"";
        }

        if (!genProc.Start())
        {
            UnityEngine.Debug.LogError("Failed to start CSProjGenerator process");
            genProc = null;
        }

        return genProc;
    }

    void OnProjectGenerated(object sender, EventArgs e)
    {
        string[] editorPath = EditorApplication.applicationPath.Split('/');
        string[] unityPath = new string[editorPath.Length - 2];
        Array.Copy(editorPath, unityPath, editorPath.Length - 2);
        string unityDir = string.Join("\\", unityPath);

        _buildProc = new Process();
        _buildProc.StartInfo.FileName = unityDir + "\\MonoDevelop\\bin\\mdtool.exe";
        //        buildProc.StartInfo.WorkingDirectory = "Assets\\Orium\\CSProjGenerator\\";
        _buildProc.StartInfo.Arguments = "build SetBundleScripts.csproj";

        _buildProc.Start();
    }

    public void GenerateAndBuild()
    {
        // TODO: Make this happen every time a script is saved?
        
        _genProc = GenerateProject();
    }
}

