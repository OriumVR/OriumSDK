﻿using System;
using System.IO;
using Microsoft.Build.Construction;

namespace CSProjGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = ProjectRootElement.Create();
            root.DefaultTargets = "Build";

            var group = root.AddPropertyGroup();

            ProjectPropertyElement configProperty = group.AddProperty("Configuration", "Debug");
            // NOT SURE IF NECESSARY
            configProperty.Condition = " '$(Configuration)' == '' ";

            ProjectPropertyElement platformProperty = group.AddProperty("Platform", "AnyCPU");
            // NOT SURE IF NECESSARY
            platformProperty.Condition = " '$(Platform)' == '' ";

            group.AddProperty("ProjectGuid", "{" + Guid.NewGuid().ToString().ToUpper() + "}");
            group.AddProperty("OutputType", "Library");
            group.AddProperty("AssemblyName", "SetBundleScripts");
            group.AddProperty("TargetFrameworkVersion", "v3.5");
            group.AddProperty("BaseDirectory", "Assets");
    //<ProductVersion>10.0.20506</ProductVersion>
    //<SchemaVersion>2.0</SchemaVersion>
    //<RootNamespace></RootNamespace>
    //<ProjectGuid>{8F8A295F-0599-3AF5-1D54-C8BC012252E6}</ProjectGuid>
    //<OutputType>Library</OutputType>
    //<AppDesignerFolder>Properties</AppDesignerFolder>
    //<AssemblyName>Assembly-CSharp</AssemblyName>
    //<TargetFrameworkVersion>v3.5</TargetFrameworkVersion>
    //<FileAlignment>512</FileAlignment>
    //<BaseDirectory>Assets</BaseDirectory>

            var debug = root.AddPropertyGroup();
            debug.Condition = " '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ";
            debug.AddProperty("DebugSymbols", "true");
            debug.AddProperty("DebugType", "full");
            debug.AddProperty("Optimize", "false");
            debug.AddProperty("OutputPath", "Assets\\SetBundle\\");
            debug.AddProperty("DefineConstants", "DEBUG;TRACE;UNITY_5;ENABLE_NEW_BUGREPORTER;ENABLE_AUDIO;ENABLE_CACHING;ENABLE_CLOTH;ENABLE_DUCK_TYPING;ENABLE_FRAME_DEBUGGER;ENABLE_GENERICS;ENABLE_HOME_SCREEN;ENABLE_IMAGEEFFECTS;ENABLE_LIGHT_PROBES_LEGACY;ENABLE_MICROPHONE;ENABLE_MULTIPLE_DISPLAYS;ENABLE_PHYSICS;ENABLE_PLUGIN_INSPECTOR;ENABLE_SHADOWS;ENABLE_SINGLE_INSTANCE_BUILD_SETTING;ENABLE_SPRITERENDERER_FLIPPING;ENABLE_SPRITES;ENABLE_SPRITE_POLYGON;ENABLE_TERRAIN;ENABLE_RAKNET;ENABLE_UNET;ENABLE_UNITYEVENTS;ENABLE_VR;ENABLE_WEBCAM;ENABLE_WWW;ENABLE_CLOUD_SERVICES;ENABLE_CLOUD_SERVICES_ADS;ENABLE_CLOUD_HUB;ENABLE_CLOUD_PROJECT_ID;ENABLE_CLOUD_SERVICES_PURCHASING;ENABLE_CLOUD_SERVICES_ANALYTICS;ENABLE_CLOUD_SERVICES_UNET;ENABLE_CLOUD_SERVICES_BUILD;ENABLE_CLOUD_LICENSE;ENABLE_EDITOR_METRICS;ENABLE_EDITOR_METRICS_CACHING;INCLUDE_DYNAMIC_GI;INCLUDE_GI;INCLUDE_IL2CPP;INCLUDE_DIRECTX12;PLATFORM_SUPPORTS_MONO;RENDER_SOFTWARE_CURSOR;ENABLE_LOCALIZATION;ENABLE_ANDROID_ATLAS_ETC1_COMPRESSION;ENABLE_EDITOR_TESTS_RUNNER;UNITY_STANDALONE_WIN;UNITY_STANDALONE;ENABLE_SUBSTANCE;ENABLE_TEXTUREID_MAP;ENABLE_RUNTIME_GI;ENABLE_MOVIES;ENABLE_NETWORK;ENABLE_CRUNCH_TEXTURE_COMPRESSION;ENABLE_LOG_MIXED_STACKTRACE;ENABLE_UNITYWEBREQUEST;ENABLE_EVENT_QUEUE;ENABLE_CLUSTERINPUT;ENABLE_WEBSOCKET_HOST;ENABLE_MONO;ENABLE_PROFILER;UNITY_ASSERTIONS;UNITY_EDITOR;UNITY_EDITOR_64;UNITY_EDITOR_WIN;CROSS_PLATFORM_INPUT");
            debug.AddProperty("ErrorReport", "prompt");
            debug.AddProperty("WarningLevel", "4");
            debug.AddProperty("NoWarn", "0169");

            // references
            var refGroup = root.AddItemGroup();
            AddReference(refGroup, "System");
            AddReference(refGroup, "System.Core");
            AddReference(refGroup, "UnityEngine", "C:/Program Files/Unity/Editor/Data/Managed/UnityEngine.dll");
            AddReference(refGroup, "UnityEngine.UI", "C:/Program Files/Unity/Editor/Data/UnityExtensions/Unity/GUISystem/UnityEngine.UI.dll");
            AddReference(refGroup, "UnityEngine.Networking", "C:/Program Files/Unity/Editor/Data/UnityExtensions/Unity/Networking/UnityEngine.Networking.dll");


            // items to compile
            AddItems(root, "Compile", args);

            //var target = root.AddTarget("Build");
            //var task = target.AddTask("Csc");
            //task.SetParameter("Sources", "@(Compile)");
            //task.SetParameter("OutputAssembly", "test.dll");

            root.AddImport("$(MSBuildToolsPath)\\Microsoft.CSharp.targets");

            root.Save("SetBundleScripts.csproj");
            Console.WriteLine(File.ReadAllText("SetBundleScripts.csproj"));
        }

        private static void AddItems(ProjectRootElement elem, string groupName, params string[] items)
        {
            var group = elem.AddItemGroup();
            foreach (var item in items)
            {
               group.AddItem(groupName, item);
            }
        }

        private static void AddReference(ProjectItemGroupElement group, string name, string hintPath = null)
        {
            ProjectItemElement itemElement = group.AddItem("Reference", name);
            if(!string.IsNullOrEmpty(hintPath))
            {
                itemElement.AddMetadata("HintPath", hintPath);
                itemElement.AddMetadata("Private", "False");
            }
        }
    }
}
